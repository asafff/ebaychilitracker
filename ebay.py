# encoding: utf-8
import os
import sys
import datetime
from optparse import OptionParser
from operator import itemgetter
import httplib2
import urllib
import re
import base64
import io, json

sys.path.insert(0, '%s/../' % os.path.dirname(__file__))



import ebaysdk
from ebaysdk.utils import getNodeText
from ebaysdk.exception import ConnectionError
from ebaysdk.trading import Connection as Trading
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/gmail-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Asafs AutoTracking number updater'

# Production Dev Keys
Pappid = "AsafKfir-TradingD-PRD-70902ebf9-f8ff1370"
Pdevid = "3fe56698-0d68-4178-905a-268639e9b213",
Pcertid = "PRD-0902ebf903ee-652c-4b27-b1ed-c022"
seller_2017_token = "AgAAAA**AQAAAA**aAAAAA**pQr+WA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AEmIahC5KHqQWdj6x9nY+seQ**grIDAA**AAMAAA**/K9xCl0fmkGw8Q1umObBcGpMEr51r+21lKJdR0Xkcs9fZWhgmAFvn0yrvJDyqjja6FkExeVqN+DePU+4zJtTeFzO43VELkxiTVbZcPWhRnoPi7jg2ZrFFHRnNgf0lJxCcr4HQKN2IOLSVhTt3U2LrwcxHU9b3psUEFgGSa8R0se7vz0GBpnC1KgD9s9udgFalM6RlFXA7YNG0CEStWJ4BEekHg3XvX2V/tHLk/9KaQIAQ0UE65/jRaD81ctcuM2OQx3IoJ7/CzVlnBP00VCGFL5ohjss2d8C12HyO8lXXco7GlvB9EUsJ/fgigD+gI3KtZf3pIVgTHbsAP38mbg+3KSuV6LodVbF1Wxcm8QIADB9IR2wlNBxHk4Tq/zquQUiCbvl8RTLvKQDgi7R0x94kTVN/ZD/aOZ0RrknYRIooSbvCWC67d8xVi3LKVyTP/zkxDZcDKKlVFrJTrHbYDCYeVLBXxwSmiI2FaezW/iv1xabo/09aPtZkZirQirdSj540Ggp1nUbAqwlO8lYTKh2dwMuFeLUITTrlcelvlS4uwD/Pm4iZqX9IgbawVVryS+r49ZU94cpcpuZVR+lJsPieemRChW+mSiJyjw7370gbga816z6c1ew1bXE7tCbqB8PtzLOidHHYN0LioZBEQjqe7RjSNviQEQQG2CIB68dk6YqA9U+3TZFQQG4G1UblXFWGM9bSiOLGM4cbDByZ08NcSpWu9/AyLAG9HIXsxXDMU8icQ6cE3XDP0SzhQXMlk6K"
top_quality_2017_token = 'AgAAAA**AQAAAA**aAAAAA**E30BWQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AGk4ugAJSGqQ6dj6x9nY+seQ**grIDAA**AAMAAA**UY/QgR2Q5KSRJENdoiUREjLUO6Z11oQQdx+xX+kPebJLF2xADpXXgeWQVekCkNQl5H4zaEfGFV909Q/EcyWBqz7WCD2EJXjKTx/NzwWEsQiWX39dkKb1kzPOUAU1h3DUgL+DSBOTSlOyU3dI+/1Ma0zT3M21AKHKyDVq92p8f4u9JO/XJlcuKDT/E3ZX12rCKl/EviegtD2znfqSllUcgAc6f5TtyU2Qw8fNMz0nGlDOEOK3LWj3WID+lg0mtjdcjFmL24ciffUByxsLQrBLuZG1Q/tKgny4E2KkhJltSKLn5aPRZTkv+bbOIjiKWZllWGR+S+0UWEXaRTnSLFLxpmkI4zCEbROeJd5jk58fNvKFOLtEY5OaI1PcJjze0YT9wg2UDdhYINQ3+XezrS67i9/30T/8EfLZh8TAEX3lm94o0UrZIErcusP5J8L/xKIzPcuwlwpt5Dt3nIRcGuslgoSQ6QwLSFncZv0IZrptXst9xhMU1vJ8FMMBWoz9fK7TA4qGcylaF2PAxkfYGSce9DKCSmhbrOftU8kEW/ey9TvVMVWCnm3P1Jd19ohbsFvfkukA2ZUecp0XpFNtjoU47VJmM0KnWLjv5f2/AA6UKfgi33k9pgA+gqzYvGPpXdn3ZtJyHiEI0qgNeEfETO34l7NvklBg3MyI5i1g0ijgICAJfnivcxffi6EqZIzJohsRjVtUBABVxIy1XwrG83BITHkC6dkUhKpolmWKT8tCskljmZqv66IfhtqwKDYMWkcL'

# Sandbox Dev Keys
Sappid = 'AsafKfir-TradingD-SBX-a08f655c9-c476ce6b'
Sdevid = '3fe56698-0d68-4178-905a-268639e9b213'
Scertid = 'SBX-08f655c97988-33de-4070-afb1-ce32'
testuser_token = 'AgAAAA**AQAAAA**aAAAAA**D3wBWQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GlDpaLqAmdj6x9nY+seQ**ZyoEAA**AAMAAA**ukqwtAn9/C5IjR3fTs3xZynJBd5CBAJQhKQDYjpWM5YkZUaxvjwbxWo3p+M3p0Qcj5ZSkBC/3abr4Sapg76otaCCs7e/yEVa/tZ+jXqwg7CZmiPzdAZUcG/+jNS4T1qVT0EZY/CzQOPclcM0gTE0gKvd8SOyUYF7NYeJXV5t2Mt6EW1JrXd/VEUsKG6xmNZ3lwQFZl/CFhMyFKZTovn9jdDA9a5/TK0ZPR6vOIQpP89V9n5oJFGKHMfbOUgINeQLqQSC78gqRD3PT65SHZOQdrRpZMEZFI/tyFRgkoP8YxBJdTPzDCOXf9nxvb91SwzY3DBOPTLIRWjjThwP6QawlOcqgJ0VtAPd4/ajdlZ3H1DMBWfE2dyKXCpAhhlmVOkjGHPbSJG3iXhkYwSIkm3Y/wFU0H9syTz384U3hEoupTpimKYQBZAa28i3SGuZUCJR9w0o6s0fq5MGQ4bEW3rcABSI5r/WNEKmjvx4XE0mca3F8axGMBjURzT1bZg7/YmrZOEzsTHwz2cYPZrij5JD9qB8/U1aIQOwPi60YEGRvVKPsJbObfJkFq/BIOhCNJ+FFg+POdgyw/tuX4n7o49FU+u6eG9sLIZo/y88WiktpjdWgLFg+gO2yaXqij5+983CCKvTY2lB6p/56FeMiPHB4y/e0yWaCJ0nPMnPvMvcE6hh8LXyvGGQ2hZsvYdBHBAYu5FMrK4pnqAbu14WXwUNW/2LFdQzVL7s1VGci/yHj34HMKgjk5xvjpJpKmU9/6jT'
# Add this at the end of the certifates domain="api.sandbox.ebay.com"

# SHOULD BE EMPTY JUST FOR TESTING
#ebayNoTrackingNames = [{'orderID': '332172239919-1403264474014', 'buyerID': 'rageingwarhulk', 'itemID': '332172239919', 'buyerName': 'brandon daniels'}, {'orderID': '332183159983-1403450231014', 'buyerID': 'rriener69', 'itemID': '332183159983', 'buyerName': 'ronald riener'}, {'orderID': '332194646333-1403462309014', 'buyerID': 'harleysmomlori', 'itemID': '332194646333', 'buyerName': 'jim kennedy'}, {'orderID': '332194646333-1403498184014', 'buyerID': 'haynes_jen', 'itemID': '332194646333', 'buyerName': 'jennifer haynes'}]
ebayNoTrackingNames = []
userID = "usa_top_quality_2017"


def get_credentials():
	"""Gets valid user credentials from storage.

	If nothing has been stored, or if the stored credentials are invalid,
	the OAuth2 flow is completed to obtain the new credentials.

	Returns:
		Credentials, the obtained credential.
	"""
	home_dir = os.path.expanduser('~')
	credential_dir = os.path.join(home_dir, '.credentials')
	if not os.path.exists(credential_dir):
		os.makedirs(credential_dir)
	credential_path = os.path.join(credential_dir,
								   'gmail-python-quickstart.json')

	store = Storage(credential_path)
	credentials = store.get()
	if not credentials or credentials.invalid:
		flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
		flow.user_agent = APPLICATION_NAME
		if flags:
			credentials = tools.run_flow(flow, store, flags)
		else: # Needed only for compatibility with Python 2.6
			credentials = tools.run(flow, store)
		print('Storing credentials to ' + credential_path)
	return credentials

def storeOrders():
	home_dir = os.path.dirname('D:\\')
	orders_dir = os.path.join(home_dir, '.orders')
	if not os.path.exists(orders_dir):
		os.makedirs(orders_dir)
	with io.open(orders_dir + '\orders.txt', 'w', encoding='utf-8') as f:
		#f.write(json.dumps(data, ensure_ascii=False))
		pass
	path = orders_dir + '\orders.txt'
	return path
	
	#return credentials

	
def getUser():
	try:
		api = Trading(appid=Pappid, devid=Pdevid, certid=Pcertid, token = top_quality_2017_token)
		response = api.execute('GetUser', {})
		assert(response.reply.Ack == 'Success')
		userDetails = response.reply.User
		userID = response.reply.User.UserID
		site = response.reply.User.Site
		fdbkScore = userDetails.FeedbackScore
		#print(response.reply)
		print ("User ID: " + userID + " User Site: " + site)
		
	except ConnectionError as e:
		print(e)
		print(e.response.dict())

		
def getEbayOrders():
	try:
	# @@@@ Global function declarations @@@@ #
		api = Trading(appid=Sappid, devid=Sdevid, certid=Scertid, token = top_quality_2017_token)
		response = api.execute('GetOrders', {'NumberOfDays': 1})
		# completedOrders = []
		# pendingOrders = []
		
		def isShipped(order):
			if hasattr(order.TransactionArray.Transaction[0].ShippingDetails, 'ShipmentTrackingDetails'):
				if hasattr(order.TransactionArray.Transaction[0].ShippingDetails.ShipmentTrackingDetails, 'ShipmentTrackingNumber'):
					shippingNumber = order.TransactionArray.Transaction[0].ShippingDetails.ShipmentTrackingDetails.ShipmentTrackingNumber
					shippingCarrier = order.TransactionArray.Transaction[0].ShippingDetails.ShipmentTrackingDetails.ShippingCarrierUsed
					# Package has been shipped
					return True
				else:
					# Package has more than one tracking numbers, But it has been shipped
					return 'Package has more than one tracking number'
			else:
				# Package hasn't shipped, check if canceled
				if orderStatus == 'Cancelled':
					return 'Order is cacncelled'
				elif orderStatus == 'Active':
					return "Buyer hasn't payed for the order or buyer canceled the order"
				else:
					# Order is completed (active), but doesn't have a tracking number
					return False
		
		
		orders = response.reply.OrderArray.Order
		
		
		for index, order in enumerate(orders):
		# @@@@ Declarations @@@@ #
			# Order Details
			orderID = order.OrderID
			orderStatus = order.OrderStatus
			#timeCreated = order.CreatedTime
			#quantityPurchased = order.TransactionArray.Transaction[0].QuantityPurchased
			#buyerCheckoutMessage = checkoutMessage(order)

			# Item Details
			itemID = order.TransactionArray.Transaction[0].Item.ItemID
			#itemAsin = order.TransactionArray.Transaction[0].Item.SKU
			itemTitle = order.TransactionArray.Transaction[0].Item.Title
			
			# Buyer and Shipping Details
			buyerName = order.ShippingAddress.Name.lower()
			buyerID = order.BuyerUserID.lower()
			
			# street1 = order.ShippingAddress.Street1
			# def checkStreet2(order):
				# street2 = order.ShippingAddress.Street2
				# if street2 is None: #If it doesn't have a street2 defined, make it an empty string
					# street2 = str('')
			# street2 = checkStreet2(order)
			# cityName = order.ShippingAddress.CityName
			# stateOrProvince = order.ShippingAddress.StateOrProvince
			# zipCode = order.ShippingAddress.PostalCode
			# phoneNumber = order.ShippingAddress.Phone
			
			# Create buyer dictionary
			orderIdInString = str(orderID)
			orderIdInString = {}
		# @@@@ Application Run @@@@ #
			if isShipped(order) == False:
				#print "Order %s in index doesn't have a tracking number" % str(index)
				orderIdInString['orderID'] = orderID
				orderIdInString['buyerID'] = buyerID
				orderIdInString['itemID'] = itemID
				orderIdInString['itemTitle'] = itemTitle
				orderIdInString['buyerName'] = buyerName
				orderIdInString['confirmationSent'] = False
				ebayNoTrackingNames.append(orderIdInString)
			
			'''
			class Order(orderID):
				def __init__(self, orderStatus, timeCreated, quantityPurchased, buyerCheckoutMessage, itemID, itemAsin, itemTitle, buyerName, street1, street2, cityName, stateOrProvince, zipCode, phoneNumber):
					self.orderStatus = orderStatus
					self.timeCreated = timeCreated
					self.quantityPurchased = quantityPurchased
					self.buyerCheckoutMessage = buyerCheckoutMessage
					self.itemID = itemID
					self.itemAsin = itemAsin
					self.itemTitle = itemTitle
					self.buyerName = buyerName
					self.street1 = street1
					self.street2 = street2
					self.cityName = cityName
					self.stateOrProvince = stateOrProvince
					self.zipCode = zipCode
					self.phoneNumber = phoneNumber
			
			
		# @@@@ Application @@@@ #
			# Check if we already have that order in our system
			if order not in pendingOrders or orderID not in completedOrders:
				pendingOrders.append(order)

		def checkoutMessage(order):
			# Check if there was a checkout message.
			if hasattr(order, 'BuyerCheckoutMessage'):
				return order.BuyerCheckoutMessage
			else:
				return False
		'''
		# print ebayNoTrackingNames
		#print ebayNoTrackingNames[0]['buyerID']
		
	except ConnectionError as e:
		print(e)
		print(e.response.dict())


def sendEbayMessage(messageType, orderID, itemId, recipientId, buyerName, itemTitle, shippingCarrier, trackingNum):
	try:
		api = Trading(appid=Pappid, devid=Pdevid, certid=Pcertid, token = top_quality_2017_token)
		feedbackLink = 'http://feedback.ebay.com/ws/eBayISAPI.dll?LeaveFeedback2&lookup_id='+itemId
		if messageType == 1: #Confirmation message
			message = {
				'ItemID' : itemId,
				'MemberMessage' : {
					'Subject' : "We have received your order!",
					'Body' : "Dear "+buyerName.title()+",\nThank you for your purchase. \nWe will ship and update the tracking number as soon as we can. \n \nMake sure to leave us a positive 5 stars feedback, since it will help us alot as a small business. \nIf there is any problem that might occur, with the shipment or the item, please contact us first before leaving negative feedback. \nWe GUARNTEE to assist you by any means. \n \nThanks for shopping with us and we hope you'll come back, \n"+userID,
					'QuestionType' : "General",
					'RecipientID' : recipientId
				}
			}
		
		elif messageType == 2: #Shipping Information Message
			message = {
				'ItemID' : itemId,
				'MemberMessage' : {
					'Subject' : "Your item has been shipped!",
					'Body' : "Dear "+buyerName.title()+",\nYour item is on it's way to you and will arrive within this week. \nHere are the shipping details of your package: \n \nItem title: "+itemTitle+"\nOrder ID: "+orderID+"\nShipping Carrier: "+shippingCarrier+"\nTracking Number: "+trackingNum+"\n \nPlease make sure to keep us updated about any concern you may have! \n \nBest regards, \n"+userID,
					'QuestionType' : "General",
					'RecipientID' : recipientId
				}
			}
		
		elif messageType == 3: #Ask for feedback message
			message = {
				'ItemID' : itemId,
				'MemberMessage' : {
					'Subject' : "Hi "+buyerName.title()+"! How's your new item?",
					'Body' : "Hi "+buyerName.title()+", \n We hope you got your order by now and you are happy with what you got! \nIn case something is wrong or you are dissatisfied, please let us know ASAP so we can help you and we will solve the problem together. \n We are trying to promote our business on eBay and your positive feedback will help us a lot: \nIf I may ask, when you have a chance, please leave some positive feedback for me as it fosters my business, I will REALLY appreciate that. \n \nHere is a link to eBays leave feedback page: \n"+feedbackLink+"\n \nIt was a pleasant experience to do business with you! \n \nBest regards, \n"+userID,
					'QuestionType' : "General",
					'RecipientID' : recipientId
				}
			}
		print message
		#api.execute('AddMemberMessageAAQToPartner', message)

		
	except ConnectionError as e:
		print(e)


def messageDecodedLink(message):
	
	trackAt_index = message.index('https://www.amazon.com/gp/css/shiptrack/')
	link = message[trackAt_index:trackAt_index+270]
	endIndex = link.index('Shipped')
	#link = message[trackAt_index:trackAt_index+endIndex]
	#link = link.strip()
	return link
		
def getAmazonWebDetails(link):
	# http = httplib2.Http()
	# content = http.request(link)[1]
	# #stripped = re.sub('<[^<]+?>', '', content.decode())
	# print content
	
	web = urllib.urlopen(link)
	readWeb = web.read()
	endOfDiv = readWeb.index('</div>')
	startIndex = readWeb.index('<div class="a-row a-spacing-top-mini a-size-small a-color-tertiary ship-track-grid-subtext">')+105
	shippingInformation = readWeb[startIndex:startIndex+69]
	endIndex = shippingInformation.index('\n')
	shippingInformation = readWeb[startIndex:startIndex+endIndex]
	shippingInformation = shippingInformation.split(',',1)
	shippingCarrier = shippingInformation[0][9:]
	trackingNum = shippingInformation[1][13:]
	return shippingCarrier, trackingNum
	#print readWeb
		
def getMessages():
	"""Shows basic usage of the Gmail API.

	Creates a Gmail API service object and outputs a list of label names
	of the user's Gmail account.
	"""
	credentials = get_credentials()
	http = credentials.authorize(httplib2.Http())
	service = discovery.build('gmail', 'v1', http=http)

	orderedMessage = service.users().messages().list(userId='me', q='from:auto-confirm@amazon.com').execute()
	shippedMessage = service.users().messages().list(userId='me', q='from:ship-confirm@amazon.com').execute()

	# message = service.users().messages().get(userId='me', id='15ba6a82d8c8aa42').execute()
	# message_raw = message['payload']['parts'][0]['body']['data']
	# message_decoded = base64.urlsafe_b64decode(str(message_raw))
	# user = 'Brianna Lehmann'
	# if message_decoded.find(user):
		# print (message_decoded)
	# else:
		# print 'found nothing'
	for message in orderedMessage["messages"][:1]:
		messageID = message['id']
		message_object = service.users().messages().get(userId='me', id=messageID).execute()
		try:
			message_raw = message_object['payload']['parts'][0]['body']['data']
			#message_raw = message_object['snippet']
			message_decoded = base64.urlsafe_b64decode(str(message_raw)).lower()
			for order in ebayNoTrackingNames:
				orderID = order['orderID']
				buyerID = order['buyerID']
				itemID = order['itemID']
				itemTitle = order['itemTitle']
				buyerName = order['buyerName']
				if buyerName in message_decoded and order['confirmationSent'] == False:
					#print message_decoded
					order['confirmationSent'] = True
					order['shippingInfoSent'] = False
					sendEbayMessage(1, orderID, itemID, buyerID, buyerName, itemTitle, '', '')
					print 'found %s at order confirmations' % buyerName
		except KeyError:
			pass
			
	for message in shippedMessage["messages"][:10]:
		messageID = message['id']
		message_object = service.users().messages().get(userId='me', id=messageID).execute()
		try:
			message_raw = message_object['payload']['parts'][0]['body']['data']
			#message_raw = message_object['snippet']
			message_decoded = base64.urlsafe_b64decode(str(message_raw)).lower()
			message_decoded_regular = base64.urlsafe_b64decode(str(message_raw))
			for order in ebayNoTrackingNames:
				orderID = order['orderID']
				buyerID = order['buyerID']
				itemID = order['itemID']
				itemTitle = order['itemTitle']
				buyerName = order['buyerName']
				if buyerName in message_decoded:# and order['shippingInfoSent'] == False: #MAKE SURE TO REMOVE THE HASHTAGd
					#print message_decoded
					order['shippingInfoSent'] = True
					link = messageDecodedLink(message_decoded_regular)
					info = getAmazonWebDetails(link)
					shippingCarrier = info[0]
					trackingNum = info[1]
					sendEbayMessage(2, orderID, itemID, buyerID, buyerName, itemTitle, shippingCarrier, trackingNum)
					print 'found %s at shipping confirmations' % buyerName
			#print message_decoded
		except KeyError:
			pass

orders_path = storeOrders()
print orders_path
getEbayOrders()
#print ebayNoTrackingNames
#getAmazonWebDetails('https://www.amazon.com/gp/r.html?C=172P9FUEK78OQ&K=AQTX3ZZERD1KT&R=231EKFUJVYZYE&T=C&U=https%3A%2F%2Fwww.amazon.com%2Fgp%2Fcss%2Fshiptrack%2Fview.html%2Fref%3Dpe_385040_215884960_TE_SIMP_typ%3Fie%3DUTF8%26addressID%3Dllkhsuknmin%26latestArrivalDate%3D1493521200%26orderID%3D114-8876322-9561051%26shipmentDate%3D1493387477%26orderingShipmentId%3D1283567157919%26packageId%3D1&A=RZRYUQIBQWL5EAZJHB90IBA0DC0A&H=8GT9XRIYSWIRW53GANN7LPZ7UYEA&ref_=pe_385040_215884960_TE_SIMP_typ')
getMessages()
#sendEbayMessage('332194646333', 'harleysmomlori')
#sendEbayMessage('332194646333', 'usa_seller_2017')